const Task = require('../models/task')

// controller function for getting all the tasks

module.exports.getAllTasks = () => {
	return Task.find({}).then( result => {
		return result;
	})
}


// creating a task
module.exports.createTask = (requestBody) => {

	// create object
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}


// deleting a task
// "id" url parameter passed from the taskRoute.js

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}




// update a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 
		// either way, you may put else
		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})
	})
}




// Activity ===========

module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return result;
		}
	})
}


module.exports.completeTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			//return error or
			return false;
		}
		result.status = "complete";
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})

	})
}