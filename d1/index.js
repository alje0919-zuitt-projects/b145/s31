// setup dependencies
const express = require('express');
const mongoose = require('mongoose');
// this allows us to use ALL the routes defined in "taskRoute.js"
const taskRoute = require('./routes/taskRoute')

// server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({ extended:true}));

// database connection
mongoose.connect("mongodb+srv://admin:admin@cluster0.ovosv.mongodb.net/batch164_todo?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
)

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the could database"));


// routes
app.use("/tasks", taskRoute);
//https://localhost:3001/tasks/




/*
Notes:
models > controllers > routes > index.js





*/

app.listen(port, () => console.log(`Now listening to port ${port}`));